const winston = require('winston')
require('winston-papertrail').Papertrail // eslint-disable-line

const host = process.env.LOG_HOST
const port = process.env.LOG_PORT

const winstonPapertrail = new winston.transports.Papertrail({host, port })

winstonPapertrail.on('error', function (err) {
  console.error(err)
})

const logger = new winston.Logger({ transports: [winstonPapertrail] })

module.exports = logger
